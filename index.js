const express = require("express");
const mongoose = require("mongoose");
const cors = require ("cors");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");
const cartRoutes = require("./routes/cart");


const app = express();

// Connect to MongoDB
mongoose.connect("mongodb+srv://dbajleyson:XCqXRTUTKYmZVob8@wdc028-course-booking.kpkh5.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true

	}
);

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());


// Defines the "users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);

// Defines the "/product" string to be included for all the course routes defined in the "product route"
app.use("/products", productRoutes);

// Defines the "/courses" string to be included for all the course routes defined in the "order route"
app.use("/orders", orderRoutes);
app.use("/cart", cartRoutes);


// Listening to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000}`)
});
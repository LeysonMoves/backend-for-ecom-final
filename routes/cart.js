const express = require("express");
const router = express.Router();
// const userController = require("../controllers/user");
const cartController = require("../controllers/cart");
const auth = require("../auth");

router.post("/myCart", async (req, res) => {
	// console.log(req);
	// provides the users ID for the getprofile controller method
		// const userData = auth.decode(req.headers.authorization);
		// const userId = userData.id.toString();
		// console.log(userId);
	cartController.getUserCart(req.body).then(resultFromController => res.send(resultFromController));

})
module.exports = router;

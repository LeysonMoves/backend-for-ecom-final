const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require ("../auth");


// Route for CREATING a product

router.post("/create",  (req, res) => {

	productController.createProduct(req.body).then(resultFromController => res.send(resultFromController));
})
// RETRIEVING ALL ACTIVE PRODUCTS
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send (resultFromController));
})

// RETRIEVING A SINGLE PRODUCT
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// ROUTE FOR UPDATING A PRODUCT INFORMATION
router.put("/:productId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// ARCHIVE A PRODUCT
router.put("/:productId/archive", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.archiveProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;



 const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require ("../auth");




// Creating an Order / Checkout by non-admin
router.post("/checkout", auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization)
	orderController.checkout(userData, req.body).then(resultFromController => res.send (resultFromController));
})



// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;

// Retrieving all orders by an admin
router.get("/all",  (req, res) => {


	orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
})
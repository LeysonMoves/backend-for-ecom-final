const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const orderController = require("../controllers/order");
const auth = require ("../auth");


// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})




// Routes for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController));
})



// Route for retrieving user details
// auth.verify method acts as a middleware to ensure that the user is logged in before they can access specific app features
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


// Route for setting a user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	userController.updateUser(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/all",(req,res)=>{
	userController.allUser().then(resultFromController => res.send(resultFromController	));
})





router.get("/MyOrders",  (req, res) => {

	userController.getMyOrders(req.body).then(resultFromController => res.send(resultFromController));
})



// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;

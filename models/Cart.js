const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
	customerId: {
		type: String },
	items: [{
		productId: {type: String},
		price: Number,
		quantity: Number,
		subtotal: Number,
		addedOn: {
			type: Date,
			default: Date.now },
		isActive: {
			type: Boolean,
			default: true },
		isCheckedOut: {
			type: Boolean,
			default: false } 
	}],
	total: Number,
})

module.exports = mongoose.model('Cart', cartSchema);

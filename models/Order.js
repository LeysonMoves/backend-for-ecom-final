const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	name : {
			type: String,
			required: [ true, "userId is required"]
		},
	totalAmount: {
		type: Number,
		required: [true, "totalAmount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	details : [{

		userId : {
			type: String,
			required: [ true, "userId is required"]
		},
		productId: {
			type: String,
			required: [ true, "productId is required"]
		}
	}
	]
	
});

module.exports = mongoose.model("Order", orderSchema);
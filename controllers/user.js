const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}


// USER REGISTRATION
module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// USER AUTHENTICATION
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then ( result => {
		if(result == null){
			return ("Incorrect email or password! Please try again.");
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}
			else{
				return false;
			}
		}
	})
}

module.exports.allUser = () => {
	return User.find({isAdmin: false}).then(result => {
		return result;
	})
}


module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

// SET USER TO ADMIN BY AN ADMIN
module.exports.updateUser = async (user, reqParams, reqBody) => {

	if(user.isAdmin){	
		let updatedUser = {
			isAdmin : reqBody.isAdmin	
			
		}

		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
			if(error){
				return false;
			}
			else{
				return ('Setting other user to admin is successful!');
			}
		})
	}else{
		return (`You have no access`);
	}
}
// ---------------

// CREATE ORDER BY NON ADMIN
//  Async await will be used in enrolling the user because we will need to update two separate documents when enrolling a user
module.exports.checkout = async (userData,data) => {

	if(userData.isAdmin){


	// Add the product ID in the orders array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the productId in the user's orders array
		user.orders.push({productId: data.productId});

		// Saves the updated information in the database
		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	// Add the user ID and product ID in the orderDetails array of the order 
	let isOrderUpdated =  await Order.findById(data.userId).then(order => {
		// Add the userId in the product's orderDetails array
		order.orderDetails.push({userId: data.userId});

		// Save the update course information in the database
		return order.save().then((order, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	
	
	// Condition that will check if the user and course documents have been updatedddd
	if(isUserUpdated && isProductUpdated){
		return true;
	}
	else{
		return false;
	}
}


else{
	return(`You are not allowed to create an order`);
	}
}

// RETRIEVING my order
module.exports.getMyOrders = async ( reqBody) => {
	console.log(reqBody)

		return User.findById(reqBody.userId).then(result => {
			return result.orders;
		})
	}
	






	

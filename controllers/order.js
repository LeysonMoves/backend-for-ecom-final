const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// CREATE ORDER
//  Async await will be used in enrolling the user because we will need to update two separate documents when enrolling a user
module.exports.checkout = async (userData, reqBody) => {

if(userData.isAdmin === false){

	// Add the product ID in the orders array of the user
	let productID = await Product.findOne({_id: reqBody.productId}).then(result => {
		return result.id
	})

	let userID = await User.findOne({_id: userData.id}).then(result => {
		return result.id
	})

	let price = await Product.findOne({_id: reqBody.productId}).then(result => {
		return result.price
	})
	let name = await Product.findOne({_id: reqBody.productId}).then(result => {
		return result.name
	})
	

	
	let newOrder = new Order ({
		name: name,
		totalAmount : price*reqBody.quantity,
		details :[{
			userId: userID,
			productId: productID,
		}] 
	})

	User.findOne({_id: userData.id}).then(result => {
		let orderSummary = {
			productId: productID
		}

		result.orders.push(orderSummary);

		return result.save().then((result, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	return newOrder.save().then((order, error) => {
		if(error){
			return false;
		}
		else{
			return ('Order creation was successful!');
		}
	})
}


else{
	return(`You are not allowed to create an order because you are an admin!`);
	}
}



// RETRIEVING ALL ORDERS BY ADMIN
module.exports.getAllOrders = async () => {
	
		return Order.find({}).then(result => {
			return result;
		})
	}

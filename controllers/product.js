const Product = require("../models/Product");

const User = require("../models/User");
const Order = require("../models/Order");

// Create a Product
module.exports.createProduct = async ( reqBody) => {
	console.log(reqBody);
	let name = reqBody.name;
	let price = reqBody.price;
	let image = reqBody.image; 
		let newProduct = new Product ({
			name : name,
			image : image,
			price : price
		})
		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	
}

// GET ALL ACTIVE PRODUCTS
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

// RETRIEVING A SINGLE PRODUCT
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}
// UPDATING A PRODUCT INFORMATION
module.exports.updateProduct = async (user, reqParams, reqBody) => {
	// Validate if user is Admin
	if(user.isAdmin){
		// Specify the fields/properties of the document to be updated
		let updatedProduct = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return ('The product has been updated!');
			}
		})
	}else{
		return (`You cannot update a product because you are not an admin!`);
	}
}

// ARCHIVE A PRODUCT
module.exports.archiveProduct = async (user, reqParams, reqBody) => {
	// Validate if user is Admin
	if(user.isAdmin){
		// Specify the fields/properties of the document to be updated
		let archivedProduct = {
			isActive : reqBody.isActive,
			
		}

		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return ('The product has been Archived');
			}
		})
	}else{
		return (`You cannot archive a product because you are not an admin!`);
	}
}
// -